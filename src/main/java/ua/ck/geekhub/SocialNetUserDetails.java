package ua.ck.geekhub;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class SocialNetUserDetails extends User {

    private Integer userID;
    private String firstName;
    private String lastName;

    public SocialNetUserDetails(String username, String password,
                                Collection<? extends GrantedAuthority> authorities,
                                Integer userID,
                                String firstName,
                                String lastName) {
        super(username, password, authorities);
        this.userID = userID;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Integer getUserID() {
        return userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
