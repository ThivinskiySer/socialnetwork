package ua.ck.geekhub.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import ua.ck.geekhub.service.ImageService;
import ua.ck.geekhub.service.UserService;

import java.io.IOException;
import java.util.Base64;


@Controller
public class FileUploadController {

    @Autowired
    private ImageService imageService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/avatar", method = RequestMethod.GET)
    @ResponseBody
    public String getUserAvatar(@RequestParam String userID) {

        return userService.getUser(Integer.parseInt(userID)).getAvatar() == null ? null :
                Base64.getEncoder().encodeToString(userService.getUser(Integer.parseInt(userID)).getAvatar().getData());

    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String fileUpload(
            @RequestParam String name,
            @RequestParam MultipartFile file,
            @RequestParam String nickName,
            ModelMap modelMap
    ) {
        if (!file.isEmpty()) {
            try {
                imageService.saveImage(name, file.getBytes(), nickName);
            } catch (IOException e) {
                modelMap.addAttribute("errorMessage", "Error uploading file!");
                return "error";
            }
        }
        return "redirect:personalPage";
    }
}
