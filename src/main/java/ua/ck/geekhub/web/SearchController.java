package ua.ck.geekhub.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.ck.geekhub.DTO.UserStorage;
import ua.ck.geekhub.domain.User;
import ua.ck.geekhub.service.SearchService;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Controller
public class SearchController {

    @Autowired
    private SearchService searchService;

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String searchPage() {
        return "search";
    }

    @RequestMapping(value = "/searchUsersByFullName", method = RequestMethod.GET)
    @ResponseBody
    public List<UserStorage> searchUsersByFullName(@RequestParam String searchString) {
        String[] criterias = searchString.split("(\\W+)");
        UserStorage foundUser;
        List<UserStorage> foundUsers = new ArrayList<>();
        for (User user : searchService.searchUsersByFullName(criterias)) {
            foundUser = new UserStorage();
            foundUser.userID = user.getId();
            foundUser.firstName = user.getFirstName();
            foundUser.lastName = user.getLastName();
            foundUser.avatar = user.getAvatar() == null ? null :
                    Base64.getEncoder().encodeToString(user.getAvatar().getData());
            foundUsers.add(foundUser);
        }
        return foundUsers;
    }
}
