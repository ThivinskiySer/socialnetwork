package ua.ck.geekhub.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.ck.geekhub.DTO.MessageStorage;
import ua.ck.geekhub.DTO.UserStorage;
import ua.ck.geekhub.domain.Message;
import ua.ck.geekhub.domain.User;
import ua.ck.geekhub.service.MessageService;
import ua.ck.geekhub.service.UserService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

@Controller
public class MessageController {

    @Autowired
    UserService userService;
    @Autowired
    private MessageService messageService;

    @RequestMapping(value = {"/dialogs"}, method = RequestMethod.GET)
    public String dialogs() {
        return "dialogs";
    }

    @RequestMapping(value = {"/createMessage"}, method = RequestMethod.POST)
    @ResponseBody
    public void createMessage(@RequestParam String authorID,
                              @RequestParam String recipientID,
                              @RequestParam String content) {
        Integer authorId = Integer.parseInt(authorID);
        Integer recipientId = Integer.parseInt(recipientID);
        messageService.createMessage(userService.getUser(authorId), userService.getUser(recipientId),
                content, new Date());
    }

    @RequestMapping(value = {"/getInterlocutors"}, method = RequestMethod.GET)
    @ResponseBody
    public List<UserStorage> getInterlocutors(@RequestParam String userID) {
        Integer userId = Integer.parseInt(userID);
        List<UserStorage> interlocutors = new ArrayList<>();
        for (User user : messageService.getInterlocutors(userId)) {
            UserStorage interlocutor = new UserStorage();
            interlocutor.userID = user.getId();
            interlocutor.lastName = user.getLastName();
            interlocutor.firstName = user.getFirstName();
            interlocutor.avatar = user.getAvatar() == null ? null :
                    Base64.getEncoder().encodeToString(user.getAvatar().getData());
            interlocutors.add(interlocutor);
        }
        return interlocutors;
    }

    @RequestMapping(value = {"/getMessages"}, method = RequestMethod.GET)
    @ResponseBody
    public List<MessageStorage> getMessages(@RequestParam String authorID,
                                            @RequestParam String recipientID) {
        List<MessageStorage> messages = new ArrayList<>();
        StringBuilder userName = new StringBuilder(32);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy HH:mm:ss");

        for (Message message : messageService.getMessages(Integer.parseInt(authorID), Integer.parseInt(recipientID))) {
            MessageStorage messageStorage = new MessageStorage();
            messageStorage.authorName = userName.append(message.getAuthor().getFirstName())
                    .append(" ")
                    .append(message.getAuthor().getLastName()).toString();
            userName.setLength(0);

            messageStorage.authorID = message.getAuthor().getId();
            messageStorage.authorAvatar = message.getAuthor().getAvatar() == null ? null :
                    Base64.getEncoder().encodeToString(message.getAuthor().getAvatar().getData());

            messageStorage.content = message.getContent();
            messageStorage.date = dateFormat.format(new Date(message.getDateTime().getTime()));
            messages.add(messageStorage);
        }
        return messages;
    }

    @RequestMapping(value = {"/checkUpdate"}, method = RequestMethod.GET)
    @ResponseBody
    public List<MessageStorage> checkUpdate(@RequestParam String lastMessageTime,
                                            @RequestParam String authorID,
                                            @RequestParam String recipientID) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy HH:mm:ss");
        List<MessageStorage> messages = new ArrayList<>();
        StringBuilder userName = new StringBuilder(32);
        Date date = null;
        try {
            date = dateFormat.parse(lastMessageTime);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        for (Message message : messageService.getNewMessages(Integer.parseInt(authorID), Integer.parseInt(recipientID), date)) {
            MessageStorage messageStorage = new MessageStorage();
            messageStorage.authorName = userName.append(message.getAuthor().getFirstName())
                    .append(" ")
                    .append(message.getAuthor().getLastName()).toString();
            userName.setLength(0);

            messageStorage.authorID = message.getAuthor().getId();
            messageStorage.authorAvatar = message.getAuthor().getAvatar() == null ? null :
                    Base64.getEncoder().encodeToString(message.getAuthor().getAvatar().getData());

            messageStorage.content = message.getContent();
            messageStorage.date = dateFormat.format(new Date(message.getDateTime().getTime()));
            messages.add(messageStorage);
        }
        return messages;
    }

}
