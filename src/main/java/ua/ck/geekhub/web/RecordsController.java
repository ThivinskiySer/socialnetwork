package ua.ck.geekhub.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.ck.geekhub.DTO.CommentStorage;
import ua.ck.geekhub.DTO.RecordStorage;
import ua.ck.geekhub.domain.Comment;
import ua.ck.geekhub.domain.Record;
import ua.ck.geekhub.service.RecordService;
import ua.ck.geekhub.service.UserService;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Controller
public class RecordsController {

    @Autowired
    private RecordService recordService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/checkUpdateRecords", method = {RequestMethod.GET})
    @ResponseBody
    public List<RecordStorage> getNewRecords(@RequestParam Integer lastRecordID,
                                             @RequestParam Integer userID) {
        List<RecordStorage> newRecordsList = new ArrayList<>();

        for (Record record : recordService.getNewRecords(lastRecordID, userID)) {
            newRecordsList.add(createRecordDTO(record));
        }
        return newRecordsList;
    }

    @RequestMapping(value = "/createRecord", method = {RequestMethod.POST})
    @ResponseBody
    public RecordStorage createRecord(@RequestParam String recordContent,
                                      @RequestParam Integer userID,
                                      @RequestParam String recordAuthor
    ) {
        Record lastRecord = recordService.createAndGetLastRecord(recordContent, userService.getUser(userID), recordAuthor);
        return createRecordDTO(lastRecord);
    }

    @RequestMapping(value = "/getWallRecords", method = {RequestMethod.GET})
    @ResponseBody
    public List<RecordStorage> getWallRecords(@RequestParam Integer userID) {
        List<RecordStorage> wallRecords = new ArrayList<>();

        for (Record record : userService.getWallRecords(userID)) {
            wallRecords.add(createRecordDTO(record));
        }
        return wallRecords;
    }

    private RecordStorage createRecordDTO(Record record) {
        RecordStorage recordDTO = new RecordStorage();
        ArrayList<CommentStorage> comments = null;
        recordDTO.setRecordID(record.getId());
        recordDTO.setRecordContent(record.getContent());
        recordDTO.setRecordAuthor(record.getAuthor());
        recordDTO.setAvatarAuthor(record.getUser().getAvatar() == null ? null :
                Base64.getEncoder().encodeToString(record.getUser().getAvatar().getData()));
        if (record.getComments() != null) {
            comments = new ArrayList<>();
            for (Comment comment : record.getComments()) {
                comments.add(new CommentStorage(comment.getContent(), comment.getAuthor()
                        , userService.getUserByNickName(comment.getAuthor()).getAvatar() == null ? null :
                        Base64.getEncoder().encodeToString(userService.getUserByNickName(comment.getAuthor()).getAvatar().getData()),
                        comment.getId(), record.getId()
                ));
            }
        }
        recordDTO.setCommentsList(comments);
        return recordDTO;
    }
}
