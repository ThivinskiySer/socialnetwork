package ua.ck.geekhub.web;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.ck.geekhub.DTO.UserStorage;
import ua.ck.geekhub.domain.User;
import ua.ck.geekhub.service.FriendsListService;
import ua.ck.geekhub.service.UserService;

import java.lang.reflect.Type;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Comparator;
import java.util.List;

@Controller
public class FriendsListController {

    @Autowired
    UserService userService;

    @Autowired
    FriendsListService friendsListService;

    @RequestMapping(value = {"/id{id}/addToFriends"}, method = RequestMethod.GET)
    @ResponseBody
    public void addToFriends(@RequestParam String toUserID,
                             @RequestParam String fromUserID) {
        Integer toUserId = Integer.parseInt(toUserID);
        Integer fromUserId = Integer.parseInt(fromUserID);
        if (isInInviteList(toUserID, fromUserID)) {
            friendsListService.addUserToFriendsList(toUserId, fromUserId);
            removeInvite(toUserID, fromUserID);
        }
    }

    @RequestMapping(value = {"/id{id}/getFriends", "/getFriends"}, method = RequestMethod.GET)
    @ResponseBody
    public List<UserStorage> getFriends(@RequestParam String userID,
                                        @RequestParam String friendsIDs) {
        List<UserStorage> friendsOfUser = new ArrayList<>();
        List<Integer> friendsList = friendsListService.getIdFriendsOfUser(Integer.parseInt(userID));
        Type listType = new TypeToken<List<Integer>>() {
        }.getType();
        List<Integer> friendsIds = new Gson().fromJson(friendsIDs, listType);

        friendsList.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });

        friendsIds.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });

        UserStorage friend;
        User friendOfUser;
        for (Integer friendId : friendsList) {
            friend = new UserStorage();
            friendOfUser = userService.getUser(friendId);
            friend.userID = friendOfUser.getId();
            friend.avatar = friendOfUser.getAvatar() == null ? null :
                    Base64.getEncoder().encodeToString(friendOfUser.getAvatar().getData());
            friend.firstName = friendOfUser.getFirstName();
            friend.lastName = friendOfUser.getLastName();
            friendsOfUser.add(friend);
        }

        if (friendsList.size() == friendsIds.size()) {
            for (int i = 0; i < friendsList.size(); i++) {
                if (!friendsList.get(i).equals(friendsIds.get(i))) {
                    return friendsOfUser;
                }
            }
            return null;
        } else {
            return friendsOfUser;
        }
    }

    @RequestMapping(value = "/isItMyFriend", method = RequestMethod.GET)
    @ResponseBody
    public Boolean isItMyFriend(Principal principal,
                                @RequestParam String friendID) {
        Integer principalID = userService.getUserByNickName(principal.getName()).getId();
        Integer checkUserID = Integer.parseInt(friendID);
        return friendsListService.isFriend(principalID, checkUserID);
    }

    @RequestMapping(value = {"/id{id}/removeFromFriends", "/removeFromFriends"}, method = RequestMethod.GET)
    @ResponseBody
    public void removeFromFriends(@RequestParam String userID,
                                  @RequestParam String friendID) {
        Integer userId = Integer.parseInt(userID);
        Integer friendId = Integer.parseInt(friendID);
        friendsListService.removeFriendFromFriendsList(userId, friendId);
    }

    @RequestMapping(value = {"/id{id}/addToInviteList", "/addToInviteList"}, method = RequestMethod.GET)
    @ResponseBody
    public void addToInviteList(@RequestParam String fromUserID,
                                @RequestParam String toUserID) {
        Integer fromUserId = Integer.parseInt(fromUserID);
        Integer toUserId = Integer.parseInt(toUserID);
        friendsListService.addUserToInviteList(fromUserId, toUserId);
    }

    @RequestMapping(value = "/id{id}/getInvitedUsers", method = RequestMethod.GET)
    @ResponseBody
    public List<UserStorage> getInvitedUsers(Principal principal,
                                             @RequestParam String usersIDs) {
        List<UserStorage> invitedUsers = new ArrayList<>();
        List<Integer> invitedList = friendsListService.getIdYourInvitedCandidates(userService.getUserByNickName(principal.getName()).getId());
        Type listType = new TypeToken<List<Integer>>() {
        }.getType();
        List<Integer> userIds = new Gson().fromJson(usersIDs, listType);

        userIds.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });

        invitedList.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });

        UserStorage invitedUser;
        User invUser;
        for (Integer id : invitedList) {
            invitedUser = new UserStorage();
            invUser = userService.getUser(id);
            invitedUser.firstName = invUser.getFirstName();
            invitedUser.lastName = invUser.getLastName();
            invitedUser.avatar = invUser.getAvatar() == null ? null :
                    Base64.getEncoder().encodeToString(invUser.getAvatar().getData());
            invitedUser.userID = invUser.getId();
            invitedUsers.add(invitedUser);
        }

        if (invitedList.size() == userIds.size()) {
            for (int i = 0; i < invitedList.size(); i++) {
                if (!invitedList.get(i).equals(userIds.get(i))) {
                    return invitedUsers;
                }
            }
            return null;
        } else {
            return invitedUsers;
        }
    }

    @RequestMapping(value = "/id{id}/getInvites", method = RequestMethod.GET)
    @ResponseBody
    public List<UserStorage> getInvites(Principal principal,
                                        @RequestParam String usersIDs) {
        List<UserStorage> invitingUsers = new ArrayList<>();
        List<Integer> invitedList = friendsListService.getIdUsersWhichInviteYou(userService.getUserByNickName(principal.getName()).getId());
        Type listType = new TypeToken<List<Integer>>() {
        }.getType();
        List<Integer> userIds = new Gson().fromJson(usersIDs, listType);

        userIds.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });

        invitedList.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 - o2;
            }
        });

        UserStorage invitingUser;
        User invUser;
        for (Integer id : friendsListService.getIdUsersWhichInviteYou(userService.getUserByNickName(principal.getName()).getId())) {
            invitingUser = new UserStorage();
            invUser = userService.getUser(id);
            invitingUser.firstName = invUser.getFirstName();
            invitingUser.lastName = invUser.getLastName();
            invitingUser.avatar = invUser.getAvatar() == null ? null :
                    Base64.getEncoder().encodeToString(invUser.getAvatar().getData());
            invitingUser.userID = invUser.getId();
            invitingUsers.add(invitingUser);
        }
        if (invitedList.size() == userIds.size()) {
            for (int i = 0; i < invitedList.size(); i++) {
                if (!invitedList.get(i).equals(userIds.get(i))) {
                    return invitingUsers;
                }
            }
            return null;
        } else {
            return invitingUsers;
        }

    }

    @RequestMapping(value = {"/id{id}/removeInvite", "/removeInvite"}, method = RequestMethod.GET)
    @ResponseBody
    public void removeInvite(@RequestParam String toUserID,
                             @RequestParam String fromUserID) {
        Integer toUserId = Integer.parseInt(toUserID);
        Integer fromUserId = Integer.parseInt(fromUserID);

        friendsListService.removeInvite(toUserId, fromUserId);
    }

    @RequestMapping(value = {"/id{id}/isInInviteList", "/isInInviteList"}, method = RequestMethod.GET)
    @ResponseBody
    public Boolean isInInviteList(@RequestParam String toUserID,
                                  @RequestParam String fromUserID) {
        Integer fromUserId = Integer.parseInt(fromUserID);
        Integer toUserId = Integer.parseInt(toUserID);
        return friendsListService.isInInviteList(fromUserId, toUserId);
    }
}
