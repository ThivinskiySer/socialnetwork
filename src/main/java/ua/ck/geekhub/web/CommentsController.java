package ua.ck.geekhub.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ua.ck.geekhub.DTO.CommentStorage;
import ua.ck.geekhub.DTO.RequestCommentsWrapper;
import ua.ck.geekhub.domain.Comment;
import ua.ck.geekhub.service.CommentService;
import ua.ck.geekhub.service.RecordService;
import ua.ck.geekhub.service.UserService;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

@Controller
public class CommentsController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private RecordService recordService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/checkUpdateComments", method = RequestMethod.POST)
    @ResponseBody
    public List<CommentStorage> getNewComments(@RequestBody RequestCommentsWrapper comments) {
        List<CommentStorage> commentStorageList = new ArrayList<>();

        for (int i = 0; i < comments.getComments().length; i++) {
            Integer recordID = Integer.parseInt(comments.getComments()[i].getRecordID());
            Integer lastCommentID = Integer.parseInt(comments.getComments()[i].getLastCommentID());
            for (Comment comment : commentService.getNewComments(recordID, lastCommentID)) {
                commentStorageList.add(createCommentDTO(comment));
            }
        }
        return commentStorageList;
    }

    @RequestMapping(value = "/createComment", method = RequestMethod.POST)
    @ResponseBody
    public CommentStorage createComment(@RequestParam String commentContent,
                                        @RequestParam String commentAuthor,
                                        @RequestParam Integer recordID) {
        Comment comment = commentService.saveAndGetLastComment(commentContent, recordService.getRecord(recordID), commentAuthor, recordID);
        return createCommentDTO(comment);
    }

    private CommentStorage createCommentDTO(Comment comment) {
        CommentStorage commentDTO = new CommentStorage();
        commentDTO.setCommentID(comment.getId());
        commentDTO.setCommentAuthor(comment.getAuthor());
        commentDTO.setRecordID(comment.getRecord().getId());
        commentDTO.setContent(comment.getContent());
        commentDTO.setAvatarAuthor(userService.getUserByNickName(comment.getAuthor()).getAvatar() == null ? null :
                Base64.getEncoder().encodeToString(userService.getUserByNickName(comment.getAuthor()).getAvatar().getData()));
        return commentDTO;
    }


}
