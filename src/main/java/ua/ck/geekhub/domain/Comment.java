package ua.ck.geekhub.domain;

import javax.persistence.*;

@Entity
@Table(name = "COMMENT")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COMMENT_ID", unique = true, nullable = false)
    private Integer id;

    @Column(name = "CONTENT", unique = false, nullable = false, length = 100000)
    private String content;

    @Column(name = "AUTHOR", unique = false, nullable = false)
    private String author;

    @ManyToOne
    private Record record;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Record getRecord() {
        return record;
    }

    public void setRecord(Record record) {
        this.record = record;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
