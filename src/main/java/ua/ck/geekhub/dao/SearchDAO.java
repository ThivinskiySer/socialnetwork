package ua.ck.geekhub.dao;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.domain.User;

import java.util.List;

@Repository
public class SearchDAO {

    @Autowired
    SessionFactory sessionFactory;

    public List<User> searchUsersByFullName(String[] criterias) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);

        for (int i = 0; i < criterias.length; i++) {
            criteria.add(Restrictions.or(
                    Restrictions.ilike("firstName", criterias[i], MatchMode.ANYWHERE),
                    Restrictions.ilike("lastName", criterias[i], MatchMode.ANYWHERE)
            ));
        }
        return criteria.list();
    }
}
