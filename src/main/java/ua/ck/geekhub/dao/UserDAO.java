package ua.ck.geekhub.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.domain.User;

import java.util.List;


@Repository
public class UserDAO {

    @Autowired
    SessionFactory sessionFactory;

    public void saveUser(User user) {
        sessionFactory.getCurrentSession().saveOrUpdate(user);
    }

    public User getUser(Integer id) {
        return (User) sessionFactory.getCurrentSession().get(User.class, id);
    }

    public List<User> getUsers() {
        return sessionFactory.getCurrentSession()
                .createCriteria(User.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                .list();
    }

    public User getUserByNickName(String nickName) {
        return (User) sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("nickName", nickName)).uniqueResult();
    }

    public void createUser(String firstName, String lastName, String email, String nickName,
                           String password, String phone, String birthday, String gender) {
        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setNickName(nickName);
        user.setPassword(password);
        user.setPhone(phone);
        user.setBirthday(birthday);
        user.setGender(gender);
        saveUser(user);

    }

    public boolean authorizationUser(String nickName, String password) {
        User user = getUserByNickName(nickName);
        return user.getPassword().equals(password);
    }

    public boolean nickNameOccupied(String nickName) {
        return !sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("nickName", nickName)).list().isEmpty();
    }

    public boolean emailOccupied(String email) {
        return !sessionFactory.getCurrentSession().createCriteria(User.class)
                .add(Restrictions.eq("email", email)).list().isEmpty();
    }


}
