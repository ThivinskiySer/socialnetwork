package ua.ck.geekhub.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.domain.Message;
import ua.ck.geekhub.domain.User;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public class MessageDAO {

    @Autowired
    SessionFactory sessionFactory;

    public void saveMessage(Message message) {
        sessionFactory.getCurrentSession().saveOrUpdate(message);
    }

    public void createMessage(User author, User recipient, String content, Date date) {
        Message message = new Message();
        message.setAuthor(author);
        message.setRecipient(recipient);
        message.setContent(content);
        message.setDateTime(new Timestamp(date.getTime()));
        saveMessage(message);

    }

    public List<Message> getMessages(Integer authorID, Integer recipientID) {
        return sessionFactory.getCurrentSession()
                .createSQLQuery("SELECT * FROM message WHERE authorId IN (:authorID, :recipientID) " +
                        "AND recipientId IN (:authorID, :recipientID)")
                .addEntity(Message.class)
                .setParameter("authorID", authorID)
                .setParameter("recipientID", recipientID)
                .list();

    }

    public List<User> getInterlocutors(Integer userID) {
        return sessionFactory.getCurrentSession()
                .createSQLQuery("SELECT * FROM user WHERE user.ID IN (SELECT recipientId FROM message " +
                        "WHERE authorId = :userID UNION SELECT authorId FROM message WHERE recipientId = :userID)")
                .addEntity(User.class)
                .setParameter("userID", userID)
                .list();

    }

    public List<Message> getNewMessages(Integer authorID, Integer recipientID, Date date) {
        return sessionFactory.getCurrentSession()
                .createSQLQuery("SELECT * FROM message WHERE authorId IN (:authorID, :recipientID) " +
                        "AND recipientId IN (:authorID, :recipientID) AND DATE_TIME > :dateTime")
                .addEntity(Message.class)
                .setParameter("authorID", authorID)
                .setParameter("recipientID", recipientID)
                .setParameter("dateTime", new Timestamp(date.getTime()))
                .list();
    }

}