package ua.ck.geekhub.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.domain.Comment;
import ua.ck.geekhub.domain.Record;

import java.util.List;


@Repository
public class CommentDAO {

    @Autowired
    SessionFactory sessionFactory;

    public void saveComment(String content, Record record, String author) {
        Comment comment = new Comment();
        comment.setContent(content);
        comment.setRecord(record);
        comment.setAuthor(author);
        sessionFactory.getCurrentSession().saveOrUpdate(comment);
    }

    public Comment getComment(Integer id) {
        return (Comment) sessionFactory.getCurrentSession().get(Comment.class, id);
    }

    public Comment getLastComment(Integer recordID) {
        String hql = "select max(id) from Comment where record.id = :recordID";
        Integer id = (Integer) sessionFactory.getCurrentSession().createQuery(hql).setInteger("recordID", recordID).uniqueResult();

        return (Comment) sessionFactory.getCurrentSession().get(Comment.class, id);
    }

    public List<Comment> getNewComments(Integer recordID, Integer lastCommentID) {
        String hql = "from Comment where record.id = :recordID and id > :lastCommentID ORDER BY id ASC";
        List<Comment> list = sessionFactory.getCurrentSession().createQuery(hql).setInteger("recordID", recordID)
                .setInteger("lastCommentID", lastCommentID).list();

        return list;
    }
}
