package ua.ck.geekhub.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.ck.geekhub.domain.Record;
import ua.ck.geekhub.domain.User;

import java.util.List;

@Repository
public class RecordDAO {

    @Autowired
    SessionFactory sessionFactory;

    public void saveRecord(Record record) {
        sessionFactory.getCurrentSession().saveOrUpdate(record);
    }

    public void createRecord(String content, User user, String author) {
        Record record = new Record();
        record.setContent(content);
        record.setUser(user);
        record.setAuthor(author);
        saveRecord(record);
    }

    public Record getRecord(Integer id) {
        return (Record) sessionFactory.getCurrentSession().get(Record.class, id);
    }

    public List<Record> getNewRecords(Integer recordID, Integer userID) {
        String hql = "from Record where id > :recordID and user.id = :userID";
        return sessionFactory.getCurrentSession().createQuery(hql).setInteger("recordID", recordID)
                .setInteger("userID", userID).list();
    }

    public Record getLastRecord(Integer userID) {
        String hql = "select max(id) from Record where user.id = :userID";
        Integer recordID = (Integer) sessionFactory.getCurrentSession().createQuery(hql).setInteger("userID", userID).uniqueResult();
        return getRecord(recordID);
    }

}
