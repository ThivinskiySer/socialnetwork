package ua.ck.geekhub.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FriendsListDAO {

    @Autowired
    SessionFactory sessionFactory;

    public void addUserToFriendsList(Integer userID, Integer friendID) {
        sessionFactory.getCurrentSession()
                .createSQLQuery("INSERT INTO tbl_friends (personId, friendId) VALUES (:userID, :friendID)")
                .setParameter("userID", userID)
                .setParameter("friendID", friendID)
                .executeUpdate();
        sessionFactory.getCurrentSession()
                .createSQLQuery("INSERT INTO tbl_friends (personId, friendId) VALUES (:friendID, :userID)")
                .setParameter("userID", userID)
                .setParameter("friendID", friendID)
                .executeUpdate();
    }

    public List<Integer> getIdFriendsOfUser(Integer userID) {
        return sessionFactory.getCurrentSession()
                .createSQLQuery("SELECT friendId FROM tbl_friends WHERE personId = :userID")
                .setParameter("userID", userID)
                .list();
    }

    public boolean isFriend(Integer userID, Integer friendID) {
        return sessionFactory.getCurrentSession()
                .createSQLQuery("SELECT friendId FROM tbl_friends WHERE personId = :userID AND friendId = :friendID")
                .setParameter("userID", userID)
                .setParameter("friendID", friendID)
                .uniqueResult() != null;
    }

    public void removeFriendFromFriendsList(Integer userID, Integer friendID) {
        sessionFactory.getCurrentSession()
                .createSQLQuery("DELETE FROM tbl_friends WHERE personId = :userID AND friendId = :friendID")
                .setParameter("userID", userID)
                .setParameter("friendID", friendID)
                .executeUpdate();
        sessionFactory.getCurrentSession()
                .createSQLQuery("DELETE FROM tbl_friends WHERE personId = :friendID AND friendId = :userID")
                .setParameter("userID", userID)
                .setParameter("friendID", friendID)
                .executeUpdate();
    }

    public void addUserToInviteList(Integer userID, Integer candidateID) {
        sessionFactory.getCurrentSession()
                .createSQLQuery("INSERT INTO candidates_friends (personId, candidateId) VALUES (:userID, :candidateID)")
                .setParameter("userID", userID)
                .setParameter("candidateID", candidateID)
                .executeUpdate();
    }

    public List<Integer> getIdYourInvitedCandidates(Integer userID) {
        return sessionFactory.getCurrentSession()
                .createSQLQuery("SELECT candidateId FROM candidates_friends WHERE personId = :userID")
                .setParameter("userID", userID)
                .list();
    }

    public List<Integer> getIdUsersWhichInviteYou(Integer userID) {
        return sessionFactory.getCurrentSession()
                .createSQLQuery("SELECT personId FROM candidates_friends WHERE candidateId = :userID")
                .setParameter("userID", userID)
                .list();
    }

    public void removeInvite(Integer toUserID, Integer fromUserID) {
        sessionFactory.getCurrentSession()
                .createSQLQuery("DELETE FROM candidates_friends WHERE personId = :fromUserID AND candidateId = :toUserID")
                .setParameter("fromUserID", fromUserID)
                .setParameter("toUserID", toUserID)
                .executeUpdate();
    }

    public boolean isInInviteList(Integer fromUserID, Integer toUserID) {
        return sessionFactory.getCurrentSession()
                .createSQLQuery("SELECT candidateId FROM candidates_friends WHERE personId = :fromUserID AND candidateId = :toUserID")
                .setParameter("fromUserID", fromUserID)
                .setParameter("toUserID", toUserID)
                .uniqueResult() != null;
    }


}
