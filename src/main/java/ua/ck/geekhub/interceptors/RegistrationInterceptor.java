package ua.ck.geekhub.interceptors;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import ua.ck.geekhub.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class RegistrationInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String problemField = "";
        if (!userService.nickNameOccupied(request.getParameter("nickName"))
                && !userService.emailOccupied(request.getParameter("email"))) {
            return true;
        }
        if (userService.nickNameOccupied(request.getParameter("nickName"))) {
            problemField += "Таке прізвисько зайнято!  ";
            request.setAttribute("problemField", problemField);
        }
        if (userService.emailOccupied(request.getParameter("email"))) {
            problemField += "Ця електронна скринька все використовується!";
            request.setAttribute("problemField", problemField);
        }
        request.setAttribute("firstName", request.getParameter("firstName"));
        request.setAttribute("lastName", request.getParameter("lastName"));
        request.setAttribute("email", request.getParameter("email"));
        request.setAttribute("nickName", request.getParameter("nickName"));
        request.setAttribute("phone", request.getParameter("phone"));
        request.getRequestDispatcher("/registration").forward(request, response);
        return false;

    }
}
