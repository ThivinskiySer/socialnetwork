package ua.ck.geekhub.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ck.geekhub.dao.SearchDAO;
import ua.ck.geekhub.domain.User;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class SearchService {

    @Autowired
    private SearchDAO searchDAO;

    public List<User> searchUsersByFullName(String[] criterias) {
        return searchDAO.searchUsersByFullName(criterias);
    }
}
