package ua.ck.geekhub.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ck.geekhub.dao.ImageDAO;
import ua.ck.geekhub.domain.Image;

import javax.transaction.Transactional;

@Service
@Transactional
public class ImageService {

    @Autowired
    private ImageDAO imageDAO;

    public void saveImage(String name, byte[] imageData, String nickName) {
        imageDAO.saveImage(name, imageData, nickName);
    }

    public Image getImage(Integer id) {
        return imageDAO.getImage(id);
    }

    public Image getUserAvatar(String nickName) {
        return imageDAO.getUserAvatar(nickName);
    }
}
