package ua.ck.geekhub.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ck.geekhub.dao.FriendsListDAO;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class FriendsListService {

    @Autowired
    private FriendsListDAO friendsListDAO;

    public List<Integer> getIdFriendsOfUser(Integer userID) {
        return friendsListDAO.getIdFriendsOfUser(userID);
    }

    public void addUserToFriendsList(Integer userID, Integer friendID) {
        friendsListDAO.addUserToFriendsList(userID, friendID);
    }

    public boolean isFriend(Integer userID, Integer friendID) {
        return friendsListDAO.isFriend(userID, friendID);
    }

    public void removeFriendFromFriendsList(Integer userID, Integer friendID) {
        friendsListDAO.removeFriendFromFriendsList(userID, friendID);
    }

    public void addUserToInviteList(Integer userID, Integer candidateID) {
        friendsListDAO.addUserToInviteList(userID, candidateID);
    }

    public List<Integer> getIdYourInvitedCandidates(Integer userID) {
        return friendsListDAO.getIdYourInvitedCandidates(userID);
    }

    public List<Integer> getIdUsersWhichInviteYou(Integer userID) {
        return friendsListDAO.getIdUsersWhichInviteYou(userID);
    }

    public void removeInvite(Integer toUserID, Integer fromUserID) {
        friendsListDAO.removeInvite(toUserID, fromUserID);
    }

    public boolean isInInviteList(Integer fromUserID, Integer toUserID) {
        return friendsListDAO.isInInviteList(fromUserID, toUserID);
    }
}
