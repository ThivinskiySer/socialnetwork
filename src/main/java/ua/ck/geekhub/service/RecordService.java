package ua.ck.geekhub.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ck.geekhub.dao.RecordDAO;
import ua.ck.geekhub.domain.Record;
import ua.ck.geekhub.domain.User;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class RecordService {

    @Autowired
    private RecordDAO recordDAO;

    public void saveRecord(Record record) {
        recordDAO.saveRecord(record);
    }

    public void createRecord(String content, User user, String author) {
        recordDAO.createRecord(content, user, author);
    }

    public Record getRecord(Integer id) {
        return recordDAO.getRecord(id);
    }

    public List<Record> getNewRecords(Integer recordID, Integer userID) {
        return recordDAO.getNewRecords(recordID, userID);
    }

    public Record getLastRecord(Integer userID) {
        return recordDAO.getLastRecord(userID);
    }

    public Record createAndGetLastRecord(String content, User user, String author) {
        createRecord(content, user, author);
        return getLastRecord(user.getId());
    }
}
