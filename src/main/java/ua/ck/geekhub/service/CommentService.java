package ua.ck.geekhub.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ck.geekhub.dao.CommentDAO;
import ua.ck.geekhub.domain.Comment;
import ua.ck.geekhub.domain.Record;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CommentService {

    @Autowired
    private CommentDAO commentDAO;

    public void saveComment(String content, Record record, String author) {
        commentDAO.saveComment(content, record, author);
    }

    public List<Comment> getNewComments(Integer recordID, Integer lastCommentID) {
        return commentDAO.getNewComments(recordID, lastCommentID);
    }

    public Comment saveAndGetLastComment(String content, Record record, String author, Integer recordID) {
        saveComment(content, record, author);
        return commentDAO.getLastComment(recordID);
    }
    public Comment getComment(Integer id) {
        return commentDAO.getComment(id);
    }
}
