package ua.ck.geekhub.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ck.geekhub.dao.MessageDAO;
import ua.ck.geekhub.domain.Message;
import ua.ck.geekhub.domain.User;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class MessageService {

    @Autowired
    private MessageDAO messageDAO;

    public void saveMessage(Message message) {
        messageDAO.saveMessage(message);
    }

    public void createMessage(User author, User recipient, String content, Date date) {
        messageDAO.createMessage(author, recipient, content, date);
    }

    public List<Message> getMessages(Integer authorID, Integer recipientID) {
        return messageDAO.getMessages(authorID, recipientID);
    }

    public List<User> getInterlocutors(Integer userID) {
        return messageDAO.getInterlocutors(userID);
    }

    public List<Message> getNewMessages(Integer authorID, Integer recipientID, Date date) {
        return messageDAO.getNewMessages(authorID, recipientID, date);
    }


}
