package ua.ck.geekhub.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.ck.geekhub.dao.UserDAO;
import ua.ck.geekhub.domain.Record;
import ua.ck.geekhub.domain.User;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserDAO userDAO;

    public boolean nickNameOccupied(String nickName) {
        return userDAO.nickNameOccupied(nickName);
    }

    public boolean emailOccupied(String email) {
        return userDAO.emailOccupied(email);
    }

    public boolean authorizationUser(String nickName, String password) {
        return userDAO.authorizationUser(nickName, password);
    }

    public User getUserByNickName(String nickName) {
        return userDAO.getUserByNickName(nickName);
    }

    public List<Record> getWallRecords(Integer id) {
        return getUser(id).getRecords();
    }

    public void saveUser(User user) {
        userDAO.saveUser(user);
    }

    public User getUser(Integer id) {
        return userDAO.getUser(id);
    }

    public List<User> getUsers() {
        return userDAO.getUsers();
    }

    public void createUser(String firstName, String lastName, String email, String nickName,
                           String password, String phone, String birthday, String gender) {
        userDAO.createUser(firstName, lastName, email, nickName, password, phone, birthday, gender);
    }
}
