package ua.ck.geekhub.DTO;


public class RequestComment {

    private String recordID;
    private String lastCommentID;

    public String getRecordID() {
        return recordID;
    }

    public void setRecordID(String recordID) {
        this.recordID = recordID;
    }

    public String getLastCommentID() {
        return lastCommentID;
    }

    public void setLastCommentID(String lastCommentID) {
        this.lastCommentID = lastCommentID;
    }
}
