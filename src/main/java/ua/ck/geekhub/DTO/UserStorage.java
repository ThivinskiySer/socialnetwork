package ua.ck.geekhub.DTO;

public class UserStorage {

    public String nickName;
    public Integer userID;
    public String avatar;
    public String firstName;
    public String lastName;
}
