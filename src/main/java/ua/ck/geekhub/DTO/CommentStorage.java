package ua.ck.geekhub.DTO;


public class CommentStorage {

    private String content;
    private String commentAuthor;
    private String avatarAuthor;
    private Integer commentID;
    private Integer recordID;


    public CommentStorage() {
    }

    public CommentStorage(String content, String commentAuthor, String avatarAuthor, Integer commentID, Integer recordID) {
        this.content = content;
        this.commentAuthor = commentAuthor;
        this.avatarAuthor = avatarAuthor;
        this.commentID = commentID;
        this.recordID = recordID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCommentAuthor() {
        return commentAuthor;
    }

    public void setCommentAuthor(String commentAuthor) {
        this.commentAuthor = commentAuthor;
    }

    public String getAvatarAuthor() {
        return avatarAuthor;
    }

    public void setAvatarAuthor(String avatarAuthor) {
        this.avatarAuthor = avatarAuthor;
    }

    public Integer getRecordID() {
        return recordID;
    }

    public void setRecordID(Integer recordID) {
        this.recordID = recordID;
    }

    public Integer getCommentID() {
        return commentID;
    }

    public void setCommentID(Integer commentID) {
        this.commentID = commentID;
    }
}
