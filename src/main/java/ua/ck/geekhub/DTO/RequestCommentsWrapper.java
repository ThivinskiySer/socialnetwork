package ua.ck.geekhub.DTO;

public class RequestCommentsWrapper {

    private RequestComment comments[];

    public RequestComment[] getComments() {
        return comments;
    }

    public void setComments(RequestComment[] comments) {
        this.comments = comments;
    }
}
