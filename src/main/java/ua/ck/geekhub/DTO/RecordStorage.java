package ua.ck.geekhub.DTO;


import java.util.List;

public class RecordStorage {

    private String recordContent;
    private List<CommentStorage> commentsList;
    private String recordAuthor;
    private Integer recordID;
    private String avatarAuthor;

    public String getRecordContent() {
        return recordContent;
    }

    public void setRecordContent(String recordContent) {
        this.recordContent = recordContent;
    }

    public List<CommentStorage> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<CommentStorage> commentsList) {
        this.commentsList = commentsList;
    }

    public String getRecordAuthor() {
        return recordAuthor;
    }

    public void setRecordAuthor(String recordAuthor) {
        this.recordAuthor = recordAuthor;
    }

    public Integer getRecordID() {
        return recordID;
    }

    public void setRecordID(Integer recordID) {
        this.recordID = recordID;
    }

    public String getAvatarAuthor() {
        return avatarAuthor;
    }

    public void setAvatarAuthor(String avatarAuthor) {
        this.avatarAuthor = avatarAuthor;
    }
}
