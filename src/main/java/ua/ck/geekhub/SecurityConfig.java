package ua.ck.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsServiceImpl userDetailsService;


    @Autowired
    public void registerGlobalAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()

                .antMatchers("/personalPage/**").access("hasRole('ROLE_USER')")
                .antMatchers("/id{\\\\d+}/friends**").access("hasRole('ROLE_USER')")
                .antMatchers("/id**").access("hasRole('ROLE_USER')")
                .antMatchers("/dialogs**").access("hasRole('ROLE_USER')")
                .and().formLogin();

        http.csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/**").permitAll()
                .anyRequest().permitAll()
                .and();

        http.formLogin()
                .defaultSuccessUrl("/personalPage")
                .loginPage("/authorization")
                .loginProcessingUrl("/j_spring_security_check")
                .failureUrl("/login_error")
                .usernameParameter("j_username")
                .passwordParameter("j_password")
                .permitAll();

        http.logout()
                .permitAll()
                .logoutUrl("/")
                .logoutSuccessUrl("/authorization")
                .invalidateHttpSession(true);

    }

}

