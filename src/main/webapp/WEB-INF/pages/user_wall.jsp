
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap-css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <title></title>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div id="navbar" class="navbar-collapse collapse">
            <jsp:include page="navbar.jsp"/>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 sidebar">
            <jsp:include page="sidebar.jsp"/>
        </div>
        <div class="col-md-5 col-md-offset-2 main">
            <div class="row placeholders">
                <jsp:include page="external_avatar_column.jsp"/>
            </div>
        </div>
        <div class="col-md-5 ">
            <jsp:include page="external_wall_column.jsp"/>
        </div>
    </div>
</div>

</body>
</html>
