<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap-css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/avatar_functions/upload_avatar_image.js"></script>
    <script type="text/javascript" src="../js/avatar_functions/show_avatar_image.js"></script>
    <script type="text/javascript" src="../js/friend_functions/getFriendsOfUser.js"></script>

</head>
<body>
<H2><sec:authentication property="principal.firstName" /> <sec:authentication property="principal.lastName" /></H2>

<p id="pageOwnerID" style="display:none">${userID}</p>

<div style="width: 250px; height: 250px">
    <img id="avatar" class="img-thumbnail" src="" style="width: 250px; height: 250px"/>

    <form id="upload_photo_form" method="post" enctype="multipart/form-data" action="/upload">
        <button type="button" class="btn btn-sm btn-default" onclick="$(this).next().click()">Замінити
            фото
        </button>
        <input type="file" name="file" id="photo" style="visibility: hidden; position: absolute;">
        <input id="file_name" type="hidden" name="name"><br/>
        <input type="hidden" name="nickName"
               value="<sec:authentication property="principal.username" />"><br/>
    </form>
</div>

</body>
</html>
