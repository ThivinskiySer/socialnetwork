
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Друзі</title>
    <link rel="stylesheet" href="../css/bootstrap-css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <link rel="shortcut icon" type="image/png" href="../images/sone.png" />
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/friend_functions/getUserFriends.js"></script>
    <script>
        $(document).ready(function () {
            getUserFriends($('#currentUserID').text());
        });
    </script>

</head>
<body>
<p id="currentUserID" style="display:none">${userID}</p>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div id="navbar" class="navbar-collapse collapse">
            <jsp:include page="navbar.jsp"/>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 sidebar">
            <jsp:include page="sidebar.jsp"/>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <div class="row placeholders">
                <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active">Список друзів</li>

                </ul>

                <div id="in_friends_list"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
