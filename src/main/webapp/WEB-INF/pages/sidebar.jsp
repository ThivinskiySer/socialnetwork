<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

</head>
<body>
<ul class="nav nav-sidebar">
    <li><a href="/id<sec:authentication property="principal.userID"/>">Моя сторінка</a></li>
    <li><a href="/id<sec:authentication property="principal.userID"/>/friends">Мої друзі</a></li>
    <li><a href="/search">Пошук друзів</a></li>
    <li><a href="/dialogs">Мої повідомлення</a></li>
</ul>
</body>
</html>
