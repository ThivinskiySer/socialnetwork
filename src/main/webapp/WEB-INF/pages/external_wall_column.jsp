<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
<head>
    <title></title>
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/wall_functions/createComment.js"></script>
    <script type="text/javascript" src="../js/wall_functions/getWallRecords.js"></script>
    <script type="text/javascript" src="../js/wall_functions/checkUpdateComments.js"></script>
    <script type="text/javascript" src="../js/wall_functions/checkUpdateRecords.js"></script>
    <script type="text/javascript" src="../js/wall_functions/printRecordsComments.js"></script>
    <script type="text/javascript" src="../js/wall_functions/printRecord.js"></script>
    <script type="text/javascript" src="../js/wall_functions/printComment.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            getWallRecords();
        });
    </script>
</head>
<body>
<p id="author" style="display:none"><sec:authentication property="principal.username"/></p>

<p id="userID" style="display:none">${userID}</p>

<div id="records">
</div>
</body>
</html>
