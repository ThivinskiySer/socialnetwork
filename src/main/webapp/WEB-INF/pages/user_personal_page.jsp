<%--

--%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap-css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <link rel="shortcut icon" type="image/png" href="../images/sone.png" />
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <title><sec:authentication property="principal.firstName" /> <sec:authentication property="principal.lastName" /></title>

</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div id="navbar" class="navbar-collapse collapse">
            <jsp:include page="navbar.jsp"/>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 sidebar">
            <jsp:include page="sidebar.jsp"/>
        </div>
        <div class="col-md-5 col-md-offset-2 main">
            <div class="row placeholders">
                <jsp:include page="internal_avatar_column.jsp"/>
            </div>
        </div>
        <div class="col-md-5 ">
            <jsp:include page="internal_wall_column.jsp"/>
        </div>
    </div>
</div>


</body>
</html>
