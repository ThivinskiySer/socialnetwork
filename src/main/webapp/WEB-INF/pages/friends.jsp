<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
<head>
    <title>Друзі</title>
    <link rel="stylesheet" href="../css/bootstrap-css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <link rel="stylesheet" href="../css/modal_window.css">
    <link rel="shortcut icon" type="image/png" href="../images/sone.png" />
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/friend_functions/getInvitedByUser.js"></script>
    <script type="text/javascript" src="../js/friend_functions/getInvites.js"></script>
    <script type="text/javascript" src="../js/friend_functions/removeInvite.js"></script>
    <script type="text/javascript" src="../js/friend_functions/addToFriends.js"></script>
    <script type="text/javascript" src="../js/friend_functions/removeFromFriends.js"></script>
    <script type="text/javascript" src="../js/friend_functions/getAllFriends.js"></script>

    <script>
        $(document).ready(function(){
            getAllFriends($("#currentUserID").text());
            $('ul.nav-pills li').click(function () {
                $('ul.nav-pills').find($('.active')).toggleClass();
                $(this).toggleClass('active');
            });
        });
    </script>
</head>
<body>
<p id="recipientID" style="display:none"></p>
<p id="currentUserID" style="display:none"><sec:authentication property="principal.userID"/></p>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div id="navbar" class="navbar-collapse collapse">
            <jsp:include page="navbar.jsp"/>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 sidebar">
            <jsp:include page="sidebar.jsp"/>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <div class="row placeholders">
                <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active">
                        <a onclick ="getAllFriends($('#currentUserID').text())">Список друзів</a></li>
                    <li role="presentation">
                        <a onclick ="getInvitedByUser()">Ви хочете подружитись</a></li>
                    <li role="presentation">
                        <a onclick ="getInvites()">З вами хочуть подружитись</a></li>
                </ul>
                <div id="invited_list"></div>
                <div id="invites_list"></div>
                <div id="in_friends_list"></div>
            </div>
        </div>
    </div>
</div>




</body>
</html>
