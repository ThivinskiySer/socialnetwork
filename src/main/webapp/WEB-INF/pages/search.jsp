
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap-css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/search_functions/searchUsersByFullName.js"></script>

    <title></title>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div id="navbar" class="navbar-collapse collapse">
            <jsp:include page="navbar.jsp"/>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 sidebar">
            <jsp:include page="sidebar.jsp"/>
        </div>
        <div class="col-md-10 col-md-offset-2 main">

            <input id="searchString" type="text" placeholder="Введіть ім`я" >
            <button type="button" class="btn btn-sm btn-primary" onclick="searchUsersByFullName()">Почати пошук</button>

            <table id="found_users" style="width: 100%"></table>
            <p id="not_found">
        </div>
    </div>
</div>

</body>
</html>