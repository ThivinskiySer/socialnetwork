
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
<head>
    <title>Повідомлення</title>
    <link rel="stylesheet" href="../css/bootstrap-css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <link rel="shortcut icon" type="image/png" href="../images/sone.png" />
    <script type="text/javascript" src="../js/jquery.min.js"></script>

    <script type="text/javascript" src="../js/message_functions/getDialogs.js"></script>
    <script type="text/javascript" src="../js/message_functions/openDialog.js"></script>
    <script type="text/javascript" src="../js/message_functions/createMessage.js"></script>
    <script type="text/javascript" src="../js/message_functions/checkUpdate.js"></script>
    <script>

        $(document).ready(function(){
            getDialogs($("#userID").text());
        });
    </script>
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div id="navbar" class="navbar-collapse collapse">
            <jsp:include page="navbar.jsp"/>
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 sidebar">
            <jsp:include page="sidebar.jsp"/>
        </div>
        <div class="col-md-10 col-md-offset-2 main">
            <div class="row placeholders">
                <p id="userID" style="display:none"><sec:authentication property="principal.userID"/></p>
                <p id="recipientID" style="display:none"></p>
                <div id="dialogs"></div>
                <div id="message" style="display:none">
                    <textarea id="message_content" rows="5" cols="150" style="resize: none;"></textarea><br/>
                    <button id="message_send_button">Відправити</button>
                </div>
                <div id="dialog"></div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
