<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap-css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/dashboard.css">
    <link rel="stylesheet" href="../css/modal_window.css">
    <script type="text/javascript" src="../js/jquery.min.js"></script>
    <script type="text/javascript" src="../js/avatar_functions/show_avatar_image.js"></script>
    <script type="text/javascript" src="../js/friend_functions/isItMyFriend.js"></script>
    <script type="text/javascript" src="../js/friend_functions/removeFromFriends.js"></script>
    <script type="text/javascript" src="../js/friend_functions/addToInviteList.js"></script>
    <script type="text/javascript" src="../js/friend_functions/isInInviteList.js"></script>
    <script type="text/javascript" src="../js/friend_functions/removeInvite.js"></script>
    <script type="text/javascript" src="../js/message_functions/modal_window.js"></script>
    <script type="text/javascript" src="../js/message_functions/createMessage.js"></script>
    <title>Sone</title>

    <h1>${firstName} ${lastName}</h1>
</head>
<body>

<p id="pageOwnerID" style="display:none">${userID}</p>
<p id="currentUserID" style="display:none"><sec:authentication property="principal.userID"/></p>
<p id="currentUserName" style="display:none"><sec:authentication property="principal.username"/></p>

<div style="width: 250px; height: 250px">
    <img id="avatar" class="img-thumbnail" src="" style="width: 250px; height: 250px"/>

</div>
<button id="friend_button" type="button" class="btn btn-lg btn-success"
        onclick="addToInviteList($('#pageOwnerID').text(), $('#currentUserID').text())"
        style="position: relative;  top: 10px;  left: -120px;">Подружитись
</button>
<div id="friends_list" style="margin:40px 0px 20px 0px; ">
    <button id="go" class="btn btn-lg btn-success" style="position: relative;  top: 10px;  left: -120px;">Написати повідомлення</button>
</div>

<div id="modal_form" style="display: none; top: 45%; opacity: 0;">
    <span id="modal_close">X</span>
        <h3>Нове повідомлення</h3>
        <p>
        <textarea id="content" rows="10" cols="60">
        </textarea>
        </p>
        <p style="text-align: center; padding-bottom: 10px;">
            <button type="button" onclick="createMessage($('#currentUserID').text(), $('#pageOwnerID').text(),
            $('#content').val())">Відправити</button>
        </p>
</div>
<div id="overlay" style="display: none;"></div>
</body>
</html>
