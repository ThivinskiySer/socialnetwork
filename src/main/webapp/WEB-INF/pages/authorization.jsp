<%--

--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<head>
    <link rel="stylesheet" href="../css/bootstrap-css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../css/signin.css">
    <title></title>
</head>
<body>
<c:url value="/j_spring_security_check" var="loginUrl"/>
<div class="container">
    <form class="form-signin" action="${loginUrl}" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input class="form-control" type="text" name="j_username">
        <input class="form-control" type="password" name="j_password">
        <button class="btn btn-lg btn-success" type="submit">Войти</button>
    </form>
    <form class="form-signin" action="/registration">
        <input class="btn btn-lg btn-info" type="submit" value="Створити аккаунт">
    </form>

</div>
</body>
</html>
