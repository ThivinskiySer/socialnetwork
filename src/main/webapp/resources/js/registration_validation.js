$(document).ready(function () {
    $("#registrationForm").validate({
        rules: {
            firstName: {
                required: true,
                minlength: 2

            },
            lastName: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            nickName: {
                required: true,
                maxlength: 20,
                minlength: 3

            },
            password: {
                required: true,
                maxlength: 16,
                minlength: 6
            },

            password_again: {
                equalTo: "#password"
            },

            phone: {
                required: true,
                phoneUKR: true
            }
        }
    });

});
