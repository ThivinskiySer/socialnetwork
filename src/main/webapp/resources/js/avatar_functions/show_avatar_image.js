$(document).ready(function () {
    var userID = $("#pageOwnerID").text();
    $.ajax({
        type: "GET",
        dataType: "text",
        url: "/avatar",
        data: {
            "userID": userID
        },
        success: function (avatar) {
            if (avatar == "") {
                $("#avatar").attr("src", "../images/noavatar.png");
            } else {
                $("#avatar").attr("src", "data:image/gif;base64," + avatar);
            }
        }
    });
});
