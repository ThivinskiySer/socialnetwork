function createComment(event) {
    var commentContent = $(event.target).closest("div.somediv").children("input.comment").val();
    var recordID = $(event.target).closest("div.somediv").children("table.comments").find("tbody tr.recordID").text();
    var commentAuthor = $("#author").text();

    $.ajax({
        url: 'createComment',
        type: 'POST',
        dataType: 'json',
        data: {
            commentContent: commentContent,
            recordID: recordID,
            commentAuthor: commentAuthor
        },
        beforeSend: function () {
            checkUpdateRecords();
            checkUpdateComments();
        },
        success: function (comment) {
            printComment(comment);
        }
    })
}
$(document).on('click', '.submit', createComment);