function printRecordsComments(records) {
    $(records).each(function (i, record) {
        printRecord(record);
        if (record.commentsList != null) {
            $.each(record.commentsList, function (i, comment) {
                printComment(comment);
            });
        }
        $('<input type="text" class="comment" placeholder="Коментар"><br/>' +
        '<input class="submit" type="button" value="Прокоментувати">').appendTo('div#' + record.recordID);
    });
}
