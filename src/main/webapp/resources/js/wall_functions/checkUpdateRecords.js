function checkUpdateRecords() {
    var lastRecordID = $(document).find("tbody tr.recordID").last().text();
    var userID = $("#userID").text();
    if (lastRecordID == "") {
        return;
    }
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: "/checkUpdateRecords",
        data: {
            "lastRecordID": lastRecordID,
            "userID": userID
        },
        success: function (records) {
            printRecordsComments(records);
        }
    });
}