function printRecord(record) {
    var innerDiv = $('<div id=' + record.recordID + ' class="somediv" style="padding-top: 20px";/>').appendTo($('#records'));
    var table = $('<table class="table table-condensed comments" />').appendTo(innerDiv);
    $("#recordContent").val("");
    var avatarSrc;
    if (record.avatarAuthor == null) {
        avatarSrc = "../images/noavatar.png";
    } else {
        avatarSrc = "data:image/gif;base64," + record.avatarAuthor;
    }
    table.append(
        '<tr>' +
        '<td rowspan="2" width="20%"><img class="img-thumbnail" src=' + avatarSrc + ' style="width: 100px; height: 100px"/></td>' +
        '<td style="background:#2EA2CC" width="80%">' + record.recordAuthor + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>' + record.recordContent + '</td>' +
        '</tr>' +
        '<tr class="recordID" style="display:none">' +
        '<td>' + record.recordID + '</td>' +
        '</tr>');
}
