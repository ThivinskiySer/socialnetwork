function getWallRecords() {
    var userID = $("#userID").text();
    $.ajax({
        url: 'getWallRecords',
        type: 'GET',
        dataType: 'json',
        data: {
            userID: userID
        },
        success: function (records) {
            printRecordsComments(records)
        }
    });
}
