function printComment(comment) {
    var table = $('div#' + comment.recordID).children("table.comments");
    $(".comment").val("");
    if (table.find("tbody tr.commentID").last().text() == comment.commentID) {
        return;
    }
    var avatarSrc;
    if (comment.avatarAuthor == null) {
        avatarSrc = "../images/noavatar.png";
    } else {
        avatarSrc = "data:image/gif;base64," + comment.avatarAuthor;
    }
    table.append(
        '<tr>' +
        '<td rowspan="2" width="20%"><img class="img-thumbnail" src=' + avatarSrc + ' style="width: 100px; height: 100px"/></td>' +
        '<td width="80%" style="background:#5bb75b">' + comment.commentAuthor + '</td>' +
        '</tr>' +
        '<tr>' +
        '<td>' + comment.content + '</td>' +
        '</tr>' +
        '<tr class="commentID" style="display:none">' +
        '<td>' + comment.commentID + '</td>' +
        '</tr>');
}
