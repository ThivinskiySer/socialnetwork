function checkUpdateComments() {
    var lastCommentID, recordID;
    var recordsArray = {
        comments: []
    };
    $("div.somediv").each(function () {
        recordID = $(this).children("table.comments").find("tbody tr.recordID").text();
        lastCommentID = $(this).children("table.comments").find("tbody tr.commentID").last().text();
        if (lastCommentID == "") {
            lastCommentID = 0;
        }
        if (recordID != "") {
            recordsArray.comments.push({
                "recordID": recordID,
                "lastCommentID": lastCommentID
            });
        }
    });
    if (recordsArray.length == 0) {
        return;
    }

    $.ajax({
        type: "POST",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        url: "/checkUpdateComments",
        data: JSON.stringify(recordsArray),
        success: function (comments) {
            if (comments == null) {
                return;
            }
            $(comments).each(function (i, comment) {
                printComment(comment);
            });
        }
    });
}
