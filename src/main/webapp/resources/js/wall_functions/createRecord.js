function createRecord() {
    var userID = $("#userID").text();
    var recordAuthor = $("#author").text();
    var recordContent = $("#recordContent").val();
    $.ajax({
        url: 'createRecord',
        type: 'POST',
        dataType: 'json',
        data: {
            userID: userID,
            recordAuthor: recordAuthor,
            recordContent: recordContent
        },
        success: function (records) {
            checkUpdateComments();
            printRecordsComments(records);
        }
    })
}
