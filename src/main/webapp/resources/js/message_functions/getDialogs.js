function getDialogs(userID) {
    var dialogsDiv = $("#dialogs").show();
    $("#dialog").hide();
    $("#message").hide();
    $.ajax({
        url: 'getInterlocutors',
        type: 'GET',
        dataType: 'json',
        data: {
            userID: userID

        },
        success: function (dialogs) {
            $(dialogs).each(function (i, user) {
                var avatarSrc;
                if (user.avatar == null) {
                    avatarSrc = "../images/noavatar.png";
                } else {
                    avatarSrc = "data:image/gif;base64," + user.avatar;
                }
                $("<table id='"+user.userID+"' style='width: 100%'/>").appendTo(dialogsDiv)
                    .append(
                    '<tr ">' +
                    '<td style="width: 15%"><img class="img-thumbnail" src=' + avatarSrc + ' style="width: 100px; height: 100px"/></td>' +
                    '<td style="width: 70%"><a href="/id'+user.userID+'"><p align="center">'+user.firstName+" "+user.lastName+'</p></a></td>' +
                    '<td style="width: 15%"><button onclick="openDialog(' + userID + ','+ user.userID + ')">Відкрити листування</button></td>' +
                    '</tr>'
                );

            });
        }
    })
}

