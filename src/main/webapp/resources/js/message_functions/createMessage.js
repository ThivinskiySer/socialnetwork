function createMessage(authorID, recipientID, content) {

    $.ajax({
        url: 'createMessage',
        type: 'POST',

        data: {
            authorID: authorID,
            recipientID: recipientID,
            content: content
        },
        success: function () {
            $('#modal_form').css('display', 'none');
            $('#overlay').fadeOut(400);
        }
    })
}
