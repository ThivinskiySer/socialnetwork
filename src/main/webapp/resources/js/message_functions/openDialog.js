function openDialog(authorID, recipientID){
    var dialogDiv = $("#dialog").show();
    var intervalID ;
    $("#message").show();
    $("#recipientID").text(recipientID);
    $("#dialogs").hide();

    $.ajax({
        type: "GET",
        dataType: "json",
        url: "getMessages",
        data: {
            "authorID": authorID,
            "recipientID": recipientID
        },
        success: function (messages) {
            $(messages).each(function (i, message) {
                var avatarSrc;
                if (message.authorAvatar == null) {
                    avatarSrc = "../images/noavatar.png";
                } else {
                    avatarSrc = "data:image/gif;base64," + message.authorAvatar;
                }
                $("<table style='width: 100%'/>").prependTo(dialogDiv)
                    .append(
                    '<tr>' +
                    '<td style="width: 15%" rowspan="4" ><img class="img-thumbnail" src=' + avatarSrc + ' style="width: 100px; height: 100px"/></td>' +
                    '<td style="width: 15%" rowspan="4" ><a href="/id'+message.authorID+'">' + message.authorName + '</a></td>' +
                    '<td style="width: 55%" rowspan="4" ><p>' + message.content+'</p></td>' +
                    '<td style="width: 15%" rowspan="4" ><p>' + message.date + '</p></td>' +
                    '</tr>'
                );
            });
            intervalID  = setInterval(function(){checkUpdate()}, 3000);
        }

    });
    $(document).on('click', 'button#message_send_button', function () {
        checkUpdate();
        createMessage(authorID,recipientID,$("#message_content").val());
        $("#message_content").val('');
    });
    $(document).ready(function(){
        $('a').on('mouseleave', function () {
            $(window).on('beforeunload', function(){
                clearInterval(intervalID );
            });
        });
    });
}
