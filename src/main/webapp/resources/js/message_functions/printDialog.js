function printDialog(messages){
    var dialogDiv = $("#dialog");
    $(messages).each(function (i, message) {

        var avatarSrc;
        if (message.authorAvatar == null) {
            avatarSrc = "../images/noavatar.png";
        } else {
            avatarSrc = "data:image/gif;base64," + message.authorAvatar;
        }

        $("<table style='width: 100%'/>").appendTo(dialogDiv)
            .append(
            '<tr ">' +
            '<td style="width: 15%" rowspan="4" ><img class="img-thumbnail" src=' + avatarSrc + ' style="width: 100px; height: 100px"/></td>' +
            '<td style="width: 15%" rowspan="4" ><a href="/id'+message.authorID+'">' + message.authorName + '</a></td>' +
            '<td style="width: 55%" rowspan="4" ><p>' + message.content+'</p></td>' +
            '<td style="width: 15%" rowspan="4" ><p>' + message.date + '</p></td>' +
            '</tr>'


        );

    });
}
