function searchUsersByFullName(){
    var searchString = $("#searchString").val();
    var usersTable = $("#found_users");
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "searchUsersByFullName",
        data: {
            "searchString": searchString
        },
        success: function (foundUsers) {
            $("#found_users").find("tr").remove();
            if (foundUsers.length == 0) {
                $("#not_found").text("Пошук не дав результатів");
            } else {
                $("#not_found").text("");
            $(foundUsers).each(function (i, user) {
                var avatarSrc;
                if (user.avatar == null) {
                    avatarSrc = "../images/noavatar.png";
                } else {
                    avatarSrc = "data:image/gif;base64," + user.avatar;
                }

                $(usersTable)
                    .append(
                    '<tr ">' +
                    '<td style="width: 15%" rowspan="3" ><img class="img-thumbnail" src=' + avatarSrc + ' style="width: 100px; height: 100px"/></td>' +
                    '<td style="width: 15%" rowspan="3" ><a href="/id' + user.userID + '">' + user.firstName + " " + user.lastName + '</a></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td><button onclick="removeFromFriends(' + user + ',' + user.userID + ')">Написати повідомлення</button></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td><a href="/id' + user.userID + '/friends"' + '>Подивитись друзів</a></td>' +
                    '</tr>'
                );

            });
        }

        }

    });
}
