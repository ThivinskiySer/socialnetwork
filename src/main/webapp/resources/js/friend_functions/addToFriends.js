function addToFriends(toUserID, fromUserID) {

    $.ajax({
        url: 'addToFriends',
        type: 'GET',

        data: {
            toUserID: toUserID,
            fromUserID: fromUserID
        },

        success: function () {
            $("#friend_button").attr("onclick", "removeFromFriends()")
                .attr("class", "btn btn-lg btn-danger").text("Роздружитись");;
        }
    })
}
