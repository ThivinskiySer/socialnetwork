$(document).ready(function () {

    var toUserID = $("#pageOwnerID").text();
    var fromUserID = $("#currentUserID").text();
    $.ajax({
        url: 'isInInviteList',
        type: 'GET',
        dataType: "json",
        data: {
            toUserID: toUserID,
            fromUserID: fromUserID
        },
        success: function (checked) {

            if (checked == true){
                $("#friend_button").attr("onclick", "removeInvite("+toUserID+","+fromUserID+")")
                    .attr("class", "btn btn-lg btn-warning").text("Відкликати запрошення");
            }
        }
    })

});