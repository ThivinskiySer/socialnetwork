function removeInvite(toUserID, fromUserID) {
    $.ajax({
        url: 'removeInvite',
        type: 'GET',
        data: {
            fromUserID: fromUserID,
            toUserID: toUserID
        },
        success: function () {
            $("#friend_button").attr("onclick", "addToInviteList("+toUserID+","+fromUserID+")")
                .attr("class", "btn btn-lg btn-success").text("Подружитись");
        }
    })
}