function addToInviteList(toUserID, fromUserID) {

    $.ajax({
        url: 'addToInviteList',
        type: 'GET',

        data: {
            toUserID: toUserID,
            fromUserID: fromUserID
        },

        success: function () {
            $("#friend_button").attr("onclick", "removeInvite("+toUserID+","+fromUserID+")")
                .attr("class", "btn btn-lg btn-warning").text("Відкликати запрошення");
        }
    })
}