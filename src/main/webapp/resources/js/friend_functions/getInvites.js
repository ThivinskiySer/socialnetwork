function getInvites(){
    var toUserID = $("#currentUserID").text();
    var invitesDiv = $("#invites_list").show();;
    $("#in_friends_list").hide();
    $("#invited_list").hide();
    var usersIDs  = [];
    invitesDiv.find("table").each(function(){
            usersIDs.push($(this).attr("id"));
        }
    );
    $.ajax({
        type: "GET",
        url: "getInvites",
        dataType: "json",
        data: {
            "usersIDs": JSON.stringify(usersIDs)
        },
        success: function (fromUsers) {

            $(fromUsers).each(function (i, user) {
                var avatarSrc;
                if (user.avatar == null) {
                    avatarSrc = "../images/noavatar.png";
                } else {
                    avatarSrc = "data:image/gif;base64," + user.avatar;
                }
                $("<table id='"+user.userID+"' style='width: 100%'/>").appendTo(invitesDiv)
                    .append(
                    '<tr ">' +
                    '<td style="width: 15%" rowspan="5" ><img class="img-thumbnail" src=' + avatarSrc + ' style="width: 100px; height: 100px"/></td>' +
                    '<td style="width: 15%" rowspan="5" ><a href="/id'+user.userID+'">'+user.firstName+" "+user.lastName+'</a></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td><button class="fr_b" onclick="removeInvite(' + toUserID + ',' + user.userID + ')">Відхилити запрошення</button></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td><button onclick="addToFriends(' + toUserID + ',' + user.userID + ')">Добавити до друзів</button></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td><a href="/id' + user.userID + '/friends"'  + '>Подивитись друзів</a></td>' +
                    '</tr>'
                );

            });
        }

    });
    $(document).on('click', '.fr_b', function () {
        $(this).parent('div').fadeOut();
    });
}

