function removeFromFriends(pageOwnerID, friendID) {
    //var pageOwnerID = $("#pageOwnerID").text();
    $.ajax({
        url: 'removeFromFriends',
        type: 'GET',
        data: {
            userID: pageOwnerID,
            friendID: friendID
        },
        success: function () {
            $("#friend_button").attr("onclick", "addToInviteList()")
                .attr("class", "btn btn-lg btn-success").text("Подружитись");
        }
    });

}
