function getInvitedByUser(){
    var fromUserID = $("#currentUserID").text();
    var invitedDiv = $("#invited_list").show();;
    $("#in_friends_list").hide();
    $("#invites_list").hide();
    var usersIDs  = [];
    invitedDiv.find("table").each(function(){
            usersIDs.push($(this).attr("id"));
        }
    );
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "getInvitedUsers",
        data: {
            "usersIDs": JSON.stringify(usersIDs)
        },
        success: function (invitedUsers) {
            $(invitedUsers).each(function (i, user) {
                var avatarSrc;
                if (user.avatar == null) {
                    avatarSrc = "../images/noavatar.png";
                } else {
                    avatarSrc = "data:image/gif;base64," + user.avatar;
                }
                $("<table id='"+user.userID+"' style='width: 100%'/>").appendTo(invitedDiv)
                    .append(
                    '<tr ">' +
                    '<td style="width: 15%" rowspan="4" ><img class="img-thumbnail" src=' + avatarSrc + ' style="width: 100px; height: 100px"/></td>' +
                    '<td style="width: 15%" rowspan="4" ><a href="/id'+user.userID+'">'+user.firstName+" "+user.lastName+'</a></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td><button onclick="removeInvite(' + user.userID + ',' + fromUserID + ')">Відкликати запрошення</button></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td><a href="/id' + user.userID + '/friends"'  + '>Подивитись друзів</a></td>' +
                    '</tr>'
                );


            });
        }

    });
}
$(document).on('click', '.fr_b', function () {
    $(this).parent('div').fadeOut();
});

