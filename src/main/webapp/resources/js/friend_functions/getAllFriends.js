function getAllFriends(userID) {

    var friendsDiv = $("#in_friends_list");
    friendsDiv.show();
    $("#invites_list").hide();
    $("#invited_list").hide();
    var friendsIDs  = [];
    friendsDiv.find("table").each(function(){
            friendsIDs.push($(this).attr("id"));
        }
    );

    $.ajax({
        type: "GET",
        dataType: "json",
        url: "getFriends",
        data: {
            "userID": userID,
            "friendsIDs": JSON.stringify(friendsIDs)
        },
        success: function (friendsOfUser) {

            $(friendsOfUser).each(function (i, friend) {
                var avatarSrc;
                if (friend.avatar == null) {
                    avatarSrc = "../images/noavatar.png";
                } else {
                    avatarSrc = "data:image/gif;base64," + friend.avatar;
                }

                $("<table id='"+friend.userID+"' style='width: 100%'/>").appendTo(friendsDiv)
                    .append(
                    '<tr ">' +
                    '<td style="width: 15%" rowspan="4" ><img class="img-thumbnail" src=' + avatarSrc + ' style="width: 100px; height: 100px"/></td>' +
                    '<td style="width: 15%" rowspan="4" ><a href="/id'+friend.userID+'">'+friend.firstName+" "+friend.lastName+'</a></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td><button class="act_but" onclick="removeFromFriends(' + userID + ',' + friend.userID + ')">Роздружитись</button></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td><a href="/id' + friend.userID + '/friends"'  + '>Подивитись друзів</a></td>' +
                    '</tr>'
                    );

            });
        }

    });
}
$(document).on('click', '.act_but', function () {
    $(this).parent('table').hide();
});

