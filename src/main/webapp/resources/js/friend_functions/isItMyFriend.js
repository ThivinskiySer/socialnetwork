$(document).ready(function () {

        var userID = $("#currentUserID").text();
        var friendID = $("#pageOwnerID").text();
        $.ajax({
            url: 'isItMyFriend',
            type: 'GET',
            dataType: "json",
            data: {
                friendID: friendID
            },

            success: function (checked) {
                if (checked == true){
                    $("#friend_button").attr("onclick", "removeFromFriends("+userID+","+friendID+")")
                        .attr("class", "btn btn-lg btn-danger").text("Роздружитись");
                }
            }
        })

});
